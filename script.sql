USE [master]
GO
/****** Object:  Database [test]    Script Date: 11/04/2022 17:11:00 ******/
CREATE DATABASE [test]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'test', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\test.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'test_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\test_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [test] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [test].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [test] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [test] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [test] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [test] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [test] SET ARITHABORT OFF 
GO
ALTER DATABASE [test] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [test] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [test] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [test] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [test] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [test] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [test] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [test] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [test] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [test] SET  DISABLE_BROKER 
GO
ALTER DATABASE [test] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [test] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [test] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [test] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [test] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [test] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [test] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [test] SET RECOVERY FULL 
GO
ALTER DATABASE [test] SET  MULTI_USER 
GO
ALTER DATABASE [test] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [test] SET DB_CHAINING OFF 
GO
ALTER DATABASE [test] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [test] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [test] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'test', N'ON'
GO
ALTER DATABASE [test] SET QUERY_STORE = OFF
GO
USE [test]
GO
/****** Object:  Table [dbo].[t001_rol]    Script Date: 11/04/2022 17:11:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t001_rol](
	[id] [int] NOT NULL,
	[description] [varchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t001_rol_usuario]    Script Date: 11/04/2022 17:11:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t001_rol_usuario](
	[id_usuario] [varchar](30) NOT NULL,
	[id_rol] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_usuario] ASC,
	[id_rol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t001_transaction]    Script Date: 11/04/2022 17:11:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t001_transaction](
	[nu_id] [int] IDENTITY(1,1) NOT NULL,
	[fe_create_on] [date] NULL,
	[in_state] [varchar](255) NULL,
	[nb_lastname] [varchar](255) NULL,
	[nb_name] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[nu_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t001_usuario]    Script Date: 11/04/2022 17:11:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t001_usuario](
	[id] [varchar](30) NOT NULL,
	[email] [varchar](255) NULL,
	[password] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[t001_transaction] ON 

INSERT [dbo].[t001_transaction] ([nu_id], [fe_create_on], [in_state], [nb_lastname], [nb_name]) VALUES (1, CAST(N'2022-05-10' AS Date), N'Activo', N'Uzcategui', N'Yorman')
INSERT [dbo].[t001_transaction] ([nu_id], [fe_create_on], [in_state], [nb_lastname], [nb_name]) VALUES (2, CAST(N'2022-05-10' AS Date), N'Inactivo', N'Desa1', N'Desarrollador')
INSERT [dbo].[t001_transaction] ([nu_id], [fe_create_on], [in_state], [nb_lastname], [nb_name]) VALUES (3, CAST(N'2022-05-09' AS Date), N'Activo', N'Postman', N'Prueba')
SET IDENTITY_INSERT [dbo].[t001_transaction] OFF
INSERT [dbo].[t001_usuario] ([id], [email], [password]) VALUES (N'test1', N'yorman2330@gmail.com', N'$2a$10$kSEM27RTffaOukNO2N9C5O0vWVq9FDM1eG4fg9NzSTeAVgQsqaDay')
SET ANSI_PADDING ON
GO
/****** Object:  Index [UK_j5pct2tisprhrwaojuvufvoke]    Script Date: 11/04/2022 17:11:01 ******/
ALTER TABLE [dbo].[t001_rol] ADD  CONSTRAINT [UK_j5pct2tisprhrwaojuvufvoke] UNIQUE NONCLUSTERED 
(
	[description] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UK_ge9765sr3d5fubjotgmonmyma]    Script Date: 11/04/2022 17:11:01 ******/
ALTER TABLE [dbo].[t001_rol_usuario] ADD  CONSTRAINT [UK_ge9765sr3d5fubjotgmonmyma] UNIQUE NONCLUSTERED 
(
	[id_rol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[t001_rol_usuario]  WITH CHECK ADD  CONSTRAINT [FK4hgta3qeen7nnbiqe3urbwxn6] FOREIGN KEY([id_rol])
REFERENCES [dbo].[t001_rol] ([id])
GO
ALTER TABLE [dbo].[t001_rol_usuario] CHECK CONSTRAINT [FK4hgta3qeen7nnbiqe3urbwxn6]
GO
ALTER TABLE [dbo].[t001_rol_usuario]  WITH CHECK ADD  CONSTRAINT [FKnkjdyhoolerigoy847ugf7j8y] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[t001_usuario] ([id])
GO
ALTER TABLE [dbo].[t001_rol_usuario] CHECK CONSTRAINT [FKnkjdyhoolerigoy847ugf7j8y]
GO
USE [master]
GO
ALTER DATABASE [test] SET  READ_WRITE 
GO
