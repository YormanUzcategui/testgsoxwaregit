package com.test.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author yuzcategui Mapeo de la tabla T001_Rol
 *
 */
@Entity
@Table(name = "T001_ROL")
public class T001_Rol {

	@Id
	@Column(name = "ID", unique = true, nullable = false, length = 30)
	private Integer id;

	@Column(name = "DESCRIPTION", unique = true, nullable = false, length = 30)
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public T001_Rol(Integer id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	public T001_Rol() {
		super();
	}
}
