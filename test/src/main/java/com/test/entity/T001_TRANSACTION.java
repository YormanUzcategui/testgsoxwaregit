package com.test.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author yuzcategui
 * Clase donde se define el modelo de la tabla T001_TRANSACTION
 *
 */
@Entity
@Table(name = "T001_TRANSACTION")
public class T001_TRANSACTION {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer nuId;
	
	@Column(name="NB_NAME")
	private String nbName;
	
	@Column(name="NB_LASTNAME")
	private String nbLastName;
	
	@Column(name="IN_STATE")
	private  String inState;

	@Column (name="FE_CREATE_ON")
	private Date feCreateOn;

	public Integer getNU_ID() {
		return nuId;
	}

	public void setNU_ID(Integer nU_ID) {
		nuId = nU_ID;
	}

	public String getNbName() {
		return nbName;
	}

	public void setNbName(String nbName) {
		this.nbName = nbName;
	}

	public String getNbLastName() {
		return nbLastName;
	}

	public void setNbLastName(String nbLastName) {
		this.nbLastName = nbLastName;
	}

	public String getInState() {
		return inState;
	}

	public void setInState(String inState) {
		this.inState = inState;
	}

	public Date getFeCreateOn() {
		return feCreateOn;
	}

	public void setFeCreateOn(Date feCreateOn) {
		this.feCreateOn = feCreateOn;
	}
	
	
}
