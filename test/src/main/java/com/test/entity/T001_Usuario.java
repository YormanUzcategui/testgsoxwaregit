package com.test.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import java.util.HashSet;
import java.util.Set;


/**
 * @author yuzcategui
 * Mapeo de la tabla T001_Usuario
 *
 */
@Entity
@Table(name = "T001_USUARIO")
public class T001_Usuario {
	
	@Id
	@Column(name = "ID", unique = true, nullable = false, length = 30)
	private String id;
	
	@Column (name="PASSWORD")
	private String password;
	
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "T001_ROL_USUARIO", 
				joinColumns = @JoinColumn(name = "ID_USUARIO", referencedColumnName = "id"), 
				inverseJoinColumns = @JoinColumn(name = "ID_ROL", referencedColumnName = "id")
			  )
	private Set<T001_Rol> rolAsignado = new HashSet<>(0);
	
	
	@Column (name="EMAIL")
	private String email;


	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Set<T001_Rol> getRolAsignado() {
		return rolAsignado;
	}


	public void setRolAsignado(Set<T001_Rol> rolAsignado) {
		this.rolAsignado = rolAsignado;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}
	
	public T001_Usuario() {
		super();
	}


	public T001_Usuario(String id, String password, Set<T001_Rol> rolAsignado, String email) {
		super();
		this.id = id;
		this.password = password;
		this.rolAsignado = rolAsignado;
		this.email = email;
	}

}
