package com.test.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.entity.T001_TRANSACTION;
import com.test.service.Service_T0001_Transaction;


/**
 * @author yuzcategui
 * Clase donde se definen los endpoint de T001Transaction
 *
 */
@RestController
@RequestMapping("api/transaction")
@CrossOrigin(origins="*")

public class T001TransactionController {
	
	@Autowired
	private Service_T0001_Transaction serviceTransaction;
	
	@GetMapping
	public List<T001_TRANSACTION> listarTransaction(){
		return serviceTransaction.getAll();
	}
	
	
	@GetMapping ("/{id}")
	public ResponseEntity<T001_TRANSACTION> findById(@PathVariable (name="id") Integer id) {
		return ResponseEntity.ok(serviceTransaction.getById(id));
	}
	
	@PostMapping ("/create")
	public ResponseEntity <T001_TRANSACTION> createTransaction(@RequestBody T001_TRANSACTION t001Transaction) {
		return new ResponseEntity<>(serviceTransaction.create(t001Transaction),HttpStatus.CREATED);
			
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<T001_TRANSACTION> updateTransaction (@RequestBody T001_TRANSACTION t001Transaction,
			@PathVariable (name="id") Integer id){
		T001_TRANSACTION transactionResponse= serviceTransaction.update(t001Transaction, id);
		
		return new ResponseEntity <>(transactionResponse, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String>deleteTransaction(@PathVariable Integer id){
		serviceTransaction.delete(id);
		return new ResponseEntity <>("Transacción Eliminada con Exito!",HttpStatus.OK);
	}
	
	
}
