package com.test.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.entity.T001_Rol;

public interface RolRepositorio extends JpaRepository<T001_Rol, Long>{

	public Optional<T001_Rol> findByDescription(String description);
	
}
