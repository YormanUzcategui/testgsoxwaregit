package com.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.entity.T001_TRANSACTION;

/**
 * @author yuzcategui
 * Clase donde se define la conexion del orm con la tabla T001_TRANSACTION
 *
 */
public interface T001_TransactionRepository extends JpaRepository<T001_TRANSACTION, Integer > {

}
