package com.test.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.entity.T001_Usuario;

public interface UsuarioRepositorio extends JpaRepository<T001_Usuario, String>{

	public Optional<T001_Usuario> findByEmail(String email);
	
	public Optional<T001_Usuario> findByIdOrEmail(String id,String email);
	
	public Optional<T001_Usuario> findById(String id);
	
	public boolean existsById(String id);
	
	public Boolean existsByEmail(String email);
	
}
