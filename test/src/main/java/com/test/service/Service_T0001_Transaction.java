package com.test.service;

import java.util.List;

import com.test.entity.T001_TRANSACTION;

/**
 * @author yuzcategui
 * Clase donde se definen los servicios a utilizar de la tabla T001_TRANSACTION
 *
 */
public interface Service_T0001_Transaction {

	public T001_TRANSACTION create(T001_TRANSACTION t001_TRANSACTION);
	
	public List <T001_TRANSACTION> getAll();
	
	public T001_TRANSACTION getById(Integer nuId);
	
	public void delete (Integer nuId);
	
	public T001_TRANSACTION update(T001_TRANSACTION t001_TRANSACTION, Integer nuId);
	
}
