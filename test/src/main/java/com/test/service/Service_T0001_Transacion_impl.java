package com.test.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.entity.T001_TRANSACTION;
import com.test.repository.T001_TransactionRepository;
import com.test.exeptions.ResourceNotFoundException;

/**
 * @author yuzcategui
 * Clase donde se implementan los servicios a utilizar de la tabla T001_TRANSACTION
 *
 */
@Service
public class Service_T0001_Transacion_impl implements Service_T0001_Transaction {

	@Autowired 
	private T001_TransactionRepository t001_TransactionRepository;
	
	//Crea un nuevo registro en T001_TRANSACTION - Devuelve el objeto crado
	@Override
	public T001_TRANSACTION create(T001_TRANSACTION T001_TRANSACTION) {
	
		T001_TRANSACTION newTransaction = t001_TransactionRepository.save(T001_TRANSACTION); 
		return newTransaction;
	}

	//Devuelve una lista de T001_TRANSACTION
	@Override
	public List<T001_TRANSACTION> getAll() {
		List <T001_TRANSACTION> listT001Transaction= t001_TransactionRepository.findAll();
		return listT001Transaction;
	}

	//Encuentra T001_TRANSACTION por id
	@Override
	public T001_TRANSACTION getById(Integer nuId) {
		T001_TRANSACTION t001Transaction= t001_TransactionRepository.findById(nuId).
				orElseThrow(() -> new ResourceNotFoundException(("No existe el registro con id : " + nuId))	);
		
		return t001Transaction;
	}
	
	//Elimina un registro de T001_TRANSACTION
	@Override
	public void delete(Integer nuId) {
		T001_TRANSACTION t001Transaction= t001_TransactionRepository.findById(nuId).
				orElseThrow(() -> new ResourceNotFoundException(("No existe el registro con id : " + nuId))	);
		
		t001_TransactionRepository.delete(t001Transaction);
		
	}

	
	// Actualiza el registro de  T001_TRANSACTION
	@Override
	public T001_TRANSACTION update(T001_TRANSACTION t001_TRANSACTION, Integer nuId) {
		T001_TRANSACTION t001Transaction = t001_TransactionRepository.findById(nuId).
				orElseThrow(() -> new ResourceNotFoundException(("No existe el registro con id : " + nuId))	);
		
		t001Transaction.setInState(t001_TRANSACTION.getInState());
		t001Transaction.setNbLastName(t001_TRANSACTION.getNbLastName());
		t001Transaction.setNbName(t001_TRANSACTION.getNbName());
		
		T001_TRANSACTION newT001Transaction = t001_TransactionRepository.save(t001Transaction);
		
		
		return newT001Transaction;
	}

}
